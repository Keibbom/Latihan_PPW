from django.urls import path, re_path, include
from django.conf.urls import url

from .views import *

urlpatterns = [
    #url(r'^$', index, name='index'),
    #url(r'^register$', register, name='register')
    path('', index, name='index'),
    path('index/', index, name='index'),
    path('register/', register, name='register'),
]